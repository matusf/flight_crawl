#!/usr/bin/env python3
'''Provides a class for aggregating data from RyanAir for further analysis
'''
from os import environ, path, stat
from datetime import datetime

import requests


class FlightCrawl(object):
    '''Class that aggregates data from RyanAir for further analysis

    config:
        Required:
            flight_info_url: str
                http://apigateway.ryanair.com/pub/v1/farefinder/3/oneWayFares
            flight_dates_url: str
                http://apigateway.ryanair.com/pub/v1/timetable/3/schedules
            api_key
                API key from: https://developer.ryanair.com
        Optional:
            logging: bool
            logging_file: str
                name of file to log
    '''
    def __init__(self, flight_info_url, flight_dates_url, api_key,
                 logging=False, logging_file='crawl_log.csv'):
        self.flight_info_url = flight_info_url
        self.flight_dates_url = flight_dates_url
        self.api_key = api_key
        self.logging = logging
        self.logging_file = logging_file

    def get_dates(self, depart, arrival):
        '''Returns list of dates of flights from <depart> to <arrival>

        Arguments
        ---------
        depart: str
            iata code of departure airport
        arrival: str
            iata code of arrival airport

        Returns
        -------
        list
            list of dates of flight departures
        '''
        url = ('{0}/{1}/{2}/availability'
               ).format(self.flight_dates_url, depart, arrival)
        return requests.get(url, params={'apikey': self.api_key}).json()

    def get_flight_info(self, date, depart, arrival, currency='EUR'):
        '''Returns all information about flight from <depart> to <arrival> on
        <date>

        Arguments
        ---------
        date: str
            departure date in format yyyy-mm-dd
        depart: str
            iata code of departure airport
        arrival: str
            iata code of arrival airport
        currency: str
            currency core

        Returns
        -------
        dict
            information abou flight
        '''
        payload = {
            'departureAirportIataCode': depart,
            'arrivalAirportIataCode': arrival,
            'outboundDepartureDateFrom': date,
            'outboundDepartureDateTo': date,
            'currency': currency,
            'apikey': self.api_key
        }
        return requests.get(self.flight_info_url, params=payload).json()

    def aggregate(self, depart, arrival, currency='EUR'):
        '''Returns flight info for every available date

        Arguments
        ---------
        depart: str
            iata code of departure airport
        arrival: str
            iata code of arrival airport
        currency: str
            currency core

        Returns
        -------
        iter
            iterator of information about availabile flights
        '''
        dates = self.get_dates(depart, arrival)
        return (self.get_flight_info(date, depart, arrival, currency)
                for date in dates)

    def save_data(self, filename, data):
        '''Saves data from FlightCrawl.aggregate to csv file

        - creates file if not exists
        - headers:
            - request date
            - iata code of departure airport
            - iata code of arrival airport
            - departure date
            - price of ticket
            - currency code

        Arguments
        ---------
        filename: str

        data: iterator of dicts
            data to save, see headers

        Returns
        -------
        None
        '''
        today = datetime.now().strftime('%Y-%m-%d')
        if not self._file_exists(filename):
            with open(filename, 'w') as f:
                print('requestDate,departureAirport,arrivalAirport,'
                      'departureDate,price,currencyCode', file=f)
        with open(filename, 'a') as f:
            for flight in data:
                for fare in flight.get('fares', []):
                    fields = [
                        today,
                        fare['outbound']['departureAirport']['iataCode'],
                        fare['outbound']['arrivalAirport']['iataCode'],
                        fare['outbound']['departureDate'],
                        fare['outbound']['price']['value'],
                        fare['outbound']['price']['currencyCode']
                    ]
                    print(','.join(map(str, fields)), file=f)

                if not flight.get('fares') and self.logging:
                    self._log(flight)

    def _file_exists(self, filename):
        '''Check if file exists

        - filename must be a file with non-zero size

        Arguments
        ---------
        filename: str
            file to chceck

        Returns
        -------
        True if file exist else False
        '''
        return path.isfile(filename) and stat(filename) != 0

    def _log(self, data):
        '''Writes data to file with a timestamp

        - creates file if does not exists
        - append to file if it does

        Arguments
        ---------
        data: any
            data to log
        '''
        mode = 'a' if self._file_exists(self.logging_file) else 'w'
        time = datetime.now().strftime('%Y-%m-%d')

        with open(self.logging_file, mode) as f:
            print('{0},{1}'.format(time, data), file=f)




if __name__ == '__main__':
    init = {
        'flight_info_url': environ['FLIGHT_INFO_URL'],
        'flight_dates_url': environ['FLIGHT_DATES_URL'],
        'logging': int(environ.get('LOGGING', 1)),
        'api_key': environ['API_KEY'],
        'logging_file': 'log_me.csv'
    }

    fc = FlightCrawl(**init)
    data = fc.aggregate('EDI', 'BTS')
    fc.save_data('data.csv', data)
    data = fc.aggregate('BTS', 'EDI')
    fc.save_data('data.csv', data)
