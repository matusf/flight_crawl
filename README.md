# FlightCrawl

**Simple** script for downloading flight info from
[RyanAir](https://www.ryanair.com) that will be user for analysis of price

## Config
```bash
export FLIGHT_INFO_URL='http://apigateway.ryanair.com/pub/v1/farefinder/3/oneWayFares'
export FLIGHT_DATES_URL='http://apigateway.ryanair.com/pub/v1/timetable/3/schedules'
export API_KEY='<your-api-key>'
```

## help(FlightCrawl)
```
class FlightCrawl(builtins.object)
 |  Class that aggregates data from RyanAir for further analysis
 |  
 |  config:
 |      Required:
 |          flight_info_url: str
 |              http://apigateway.ryanair.com/pub/v1/farefinder/3/oneWayFares
 |          flight_dates_url: str
 |              http://apigateway.ryanair.com/pub/v1/timetable/3/schedules
 |          api_key
 |              API key from: https://developer.ryanair.com
 |      Optional:
 |          logging: bool
 |          logging_file: str
 |              name of file to log
 |  
 |  Methods defined here:
 |  
 |  __init__(self, flight_info_url, flight_dates_url, api_key, logging=False, logging_file='crawl_log.csv')
 |      Initialize self.  See help(type(self)) for accurate signature.
 |  
 |  aggregate(self, depart, arrival, currency='EUR')
 |      Returns flight info for every availabile date
 |      
 |      Arguments
 |      ---------
 |      depart: str
 |          iata code of departure airport
 |      arrival: str
 |          iata code of arrival airport
 |      currency: str
 |          currency core
 |      
 |      Returns
 |      -------
 |      iter
 |          iterator of information about available flights
 |  
 |  get_dates(self, depart, arrival)
 |      Returns list of dates of flights from <depart> to <arrival>
 |      
 |      Arguments
 |      ---------
 |      depart: str
 |          iata code of departure airport
 |      arrival: str
 |          iata code of arrival airport
 |      
 |      Returns
 |      -------
 |      list
 |          list of dates of flight departures
 |
 |  get_flight_info(self, date, depart, arrival, currency='EUR')
 |      Returns all information about flight from <depart> to <arrival> on
 |      <date>
 |      
 |      Arguments
 |      ---------
 |      date: str
 |          departure date in format yyyy-mm-dd
 |      depart: str
 |          iata code of departure airport
 |      arrival: str
 |          iata code of arrival airport
 |      currency: str
 |          currency core
 |      
 |      Returns
 |      -------
 |      dict
 |          information abou flight
 |  
 |  save_data(self, filename, data)
 |      Saves data from FlightCrawl.aggregate to csv file
 |      
 |      - creates file if not exists
 |      - headers:
 |          - request date
 |          - iata code of departure airport
 |          - iata code of arrival airport
 |          - departure date
 |          - price of ticket
 |          - currency code
 |      
 |      Arguments
 |      ---------
 |      filename: str
 |      
 |      data: iterator of dicts
 |          data to save, see headers
 |      
 |      Returns
 |      -------
 |      None
 |  
 |  ----------------------------------------------------------------------
```
